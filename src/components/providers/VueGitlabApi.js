/**
Forked from project: vue-gitlab-api, https://gitlab.com/clorichel/vue-gitlab-api

The MIT License (MIT)

Copyright (c) 2016 Pierre-Alexandre Clorichel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

(function () {
  function install (Vue, options) {
    // default GitLab instance url, without trailing slash
    var url = 'https://gitlab.com'
    // GitLab user Private Token or Personal Access Token
    var token = 'you MUST provide an user Private Token or Personal Access Token'
    var headers = {
      'PRIVATE-TOKEN': token
    }
    // Gitlab api version
    var apiVersion = 'v4'
    // reading options
    if (typeof options !== 'undefined') {
      // a token is mandatory to connect to GitLab API,
      // but it could be set later with .setToken
      if (typeof options.token !== 'undefined') {
        token = options.token
        headers['PRIVATE-TOKEN'] = token
      }

      // an url may not be mandatory if user runs GitLab from localhost
      if (typeof options.url !== 'undefined') {
        // sanitizing url coming from options with no trailing slash
        url = options.url.replace(/\/$/, '')
      }
    }

    // GitLab API full url
    var apiUrl = url + '/api/' + apiVersion

    // adding an instance method by attaching it to Vue.prototype
    /**
     * A deadly simple Vue.js plugin to consume GitLab API.
     *
     * @mixin
     * @author {@link http://clorichel.com Pierre-Alexandre Clorichel}
     * @license {@link https://gitlab.com/clorichel/vue-gitlab-api/blob/master/LICENSE MIT License}
     * @see {@link https://gitlab.com/clorichel/vue-gitlab-api Project repository on GitLab.com}
     * @see {@link http://clorichel.com http://clorichel.com}
     */
    const GitLabAPI = {
      /**
       * Set application wide GitLabAPI url value
       * @param {String} newUrl The new GitLab URL value
       * @example
       * // from anywhere in the global Vue scope
       * Vue.GitLabAPI.setUrl('https://your.gitlab-instance.com')
       *
       * // from a .vue component
       * this.GitLabAPI.setUrl('https://your.gitlab-instance.com')
       */
      setUrl: function (newUrl) {
        // sanitizing url with no trailing slash
        if (typeof newUrl === 'undefined') {
          console.warn('[vue-gitlab-api] This GitLab instance URL may not be correct')
          url = ''
        } else {
          url = newUrl.replace(/\/$/, '')
        }
        apiUrl = url + '/api/' + apiVersion
      },

      /**
       * Set application wide GitLabAPI token value
       * @param {String} newToken The new token value
       * @example
       * // from anywhere in the global Vue scope
       * Vue.GitLabAPI.setToken('your user token')
       *
       * // from a .vue component
       * this.GitLabAPI.setToken('your user token')
       */
      setToken: function (newToken) {
        if (typeof newToken === 'undefined' || newToken == null || newToken === '') {
          console.error('[vue-gitlab-api] You MUST provide a non empty Private Token or Personal Access Token')
          return
        }
        token = newToken
        headers['PRIVATE-TOKEN'] = token
      },

      /**
       * Set application wide GitLabAPI api versoin
       * @param {(String|Number)} versionNumber The new GitLab api version, default is 'v4'
       * @example
       * // from anywhere in the global Vue scope
       * Vue.GitLabAPI.setVersion('v3')
       *
       * // from a .vue component
       * this.GitLabAPI.setVersion(3)
       */
      setVersion: function (versionNumber) {
        if (typeof versionNumber === 'number' && versionNumber > 0) {
          apiVersion = 'v' + versionNumber
        } else if (typeof versionNumber === 'string' && /^v\d$/.test(versionNumber)) {
          apiVersion = versionNumber
        } else {
          console.error('[vue-gitlab-api] You MUST provide a non empty valid version number')
          return
        }
        apiUrl = url + '/api/' + apiVersion
      },

      /**
       * A request callback function.
       *
       * @callback requestCallback
       * @param {Object} response Full response from GitLab API
       */

      /**
       * A request error callback function.
       *
       * @callback errorCallback
       * @param {Object} response Full response from GitLab API
       */

      /**
       * Issue a GET request on 'GitLabAPI_url/api/v4' with params and a variable to fill in
       * @param  {String}                  uri     The GitLab API uri to consume such as '/projects'
       * @param  {Object}                  params  A parameters object such as { 'project_id': 72 }
       * @param  {(Array|requestCallback)} fillIn  The Vue.js defined data to fill in with results from GitLab API, or a callback function fed with full response
       * @param  {errorCallback}           errorCb A callback function in case of error (response is passed to callback)
       * @example
       * // -------------------------------------------------
       * // 1- With an array to fill in a Vue.js defined data
       * // -------------------------------------------------
       * // from anywhere in the global Vue scope
       * Vue.GitLabAPI.get('/projects', {}, [this.myGitLabData, 'projects'])
       * // from a .vue component
       * this.GitLabAPI.get('/projects', {}, [this.myGitLabData, 'projects'])
       *
       *
       * // ----------------------------------------------------------
       * // 2- With a callback function to play with the full response
       * // ----------------------------------------------------------
       * // from anywhere in the global Vue scope
       * Vue.GitLabAPI.get('/projects', {}, (response) => {
       *   console.log('got response:', response)
       * })
       * // from a .vue component
       * this.GitLabAPI.get('/projects', {}, (response) => {
       *   console.log('got response:', response)
       * })
       */
      get: function (uri, params, fillIn, errorCb) {
        return this._request(uri, params, 'get', fillIn, errorCb)
      },
      getAsync: function (url) {
        return this.requestAsync(url, 'get')
      },
      requestAsync: function (url, method) {
        return new Promise((resolve, reject) => {
          this._request(url, [], method, (response) => {
            resolve(response.data)
          }, (response) => {
            reject()
          })
        })
      },

      /**
       * Issue a POST request on 'GitLabAPI_url/api/v4' with params and a variable to fill in
       * @param  {String}                  uri     The GitLab API uri to consume such as '/projects'
       * @param  {Object}                  params  A parameters object such as { 'project_id': 72 }
       * @param  {(Array|requestCallback)} fillIn  The Vue.js defined data to fill in with results from GitLab API, or a callback function fed with full response
       * @param  {errorCallback}           errorCb A callback function in case of error (response is passed to callback)
       * @example
       * // from anywhere in the global Vue scope
       * Vue.GitLabAPI.post('/projects/YOUR_PROJECT_ID/issues', {
       *   title: 'My new issues from vue-gitlab-api'
       * }, (response) => { console.log('posted it!', response) })
       *
       * // from a .vue component
       * this.GitLabAPI.post('/projects/YOUR_PROJECT_ID/issues', {
       *   title: 'My new issues from vue-gitlab-api'
       * }, (response) => { console.log('posted it!', response) })
       */
      post: function (uri, params, fillIn, errorCb) {
        this._request(uri, params, 'post', fillIn, errorCb)
      },
      put: function (uri, params, fillIn, errorCb) {
        this._request(uri, params, 'put', fillIn, errorCb)
      },
      delete: function (uri, params, fillIn, errorCb) {
        this._request(uri, params, 'delete', fillIn, errorCb)
      },
      /**
       * Register your application Vuex store to improve it with GitLabAPI Vuex store module
       * @param  {Object} store Your application Vuex store
       * @example
       * // from within a .vue component
       * this.GitLabAPI.registerStore(this.$store)
       */
      registerStore: function (store) {
        if (typeof store === 'undefined') {
          console.error('[vue-gitlab-api] This do not appear to be a Vuex store')
          return
        }

        // registering GitLabAPI Vuex module
        store.registerModule('GitLabAPI', {
          state: {
            // is GitLabAPI currently downloading?
            downloading: false,
            // which what method GitlabApi is currently downloading?
            requestMethod: '',
            // how many downloads are running
            running: 0,
              // make api for vuex available, to use it in actions
            api: GitLabAPI
          },
          mutations: {
            downloading: function (state, requestMethod) {
              // currently downloading
              state.running++
              state.downloading = true
              state.requestMethod = requestMethod
            },
            downloaded: function (state) {
              // stopped downloading
              state.running--
              state.requestMethod = ''
              if (state.running === 0) {
                state.downloading = false
              }
            }
          }
        })

        // will need to update it later
        this._vuexStore = store
      },

      /**
       * If attached, the application wide Vuex store
       * @type {Object}
       * @private
       */
      _vuexStore: null,

      /**
       * Update the Vuex store if any
       * @param  {String} mutation The mutation to commit (downloading or downloaded)
       * @param  {String=} method Optional, the that is used for downloading/uploading
       * @private
       */
      _updateStore: function (mutation, method) {
        if (this._vuexStore != null) {
          this._vuexStore.commit(mutation, method)
        }
      },
      _request: function (uri, params, method, fillIn, errorCb) {
        // verifying what user sends to fill in
        if (this._verifyFillIn(fillIn) !== true) {
          return
        }

        // ensure leading slash on uri
        uri = uri.replace(/^\/?/, '/')

        // downloading starts now
        this._updateStore('downloading', method)

        // request it with headers an params
        let request
        if (method === 'get' || method === 'delete') {
          request = Vue.http[method](
            apiUrl + uri,
          {headers, params}
          )
        } else {
          request = Vue.http[method](
            apiUrl + uri,
            params,
            {headers}
          )
        }

        request.then((response) => {
          // no more downloading
          this._updateStore('downloaded')

          if (typeof fillIn === 'function') {
            // sending back the full response
            fillIn(response)
          } else {
            // fill in the data from the response body
            Vue.set(fillIn[0], fillIn[1], response.body)
          }
        }, (response) => {
          // no more downloading
          this._updateStore('downloaded')

          if (typeof errorCb === 'function') {
            // user defined an error callback function, calling it with response
            errorCb(response)
          } else {
            // no errorCb function defined, default to console error
            console.error('[vue-gitlab-api] ' + method + ' ' + uri + ' failed: "' + response.body.message + '" on ' + apiUrl + ' (using token "' + token + '")')
          }
        })
      },

      /**
       * Verifying what to fill in from API consuming functions
       * @param  {(Array|requestCallback)} fillIn  The Vue.js defined data to fill in with results from GitLab API, or a callback function fed with full response
       * @return {Boolean}                         True if the fillIn type is ok, false (with a console error) if not
       * @private
       */
      _verifyFillIn: function (fillIn) {
        // fillIn can be a callback function to which the response will be sent
        if (typeof fillIn !== 'function') {
          // variable to fill in MUST be defined, as a Vue data (to be reactive),
          // and user MUST provide the key to fill in within that data, examples:
          // - [this.dataPropertyName, 'keyToFillDataIn']
          // - [this.GitLab, 'projects']
          if (!(fillIn instanceof Array) || fillIn.length < 2) {
            console.error('[vue-gitlab-api] You MUST define the Vue data you want to fill as a two values array')
            return false
          }

          // ensure reactive data is not an array but an object, or Vue.set will
          // fail in indexing the data to the expected key
          if (Array.isArray(fillIn[0]) || typeof fillIn[0] !== 'object') {
            console.error('[vue-gitlab-api] Your Vue data to fill MUST be an object (ie `{}`)')
            return false
          }
        }

        return true
      }
    }
    Vue.prototype.GitLabAPI = GitLabAPI
  }

  if (typeof exports === 'object') {
    module.exports = install
  }
})()
