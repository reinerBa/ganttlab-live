import moment from 'moment'
import $ from 'jquery'

export default function (headerRightAppend = '') {
  $('#chartp').fullCalendar('destroy')

  return {
    schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
    lang: 'de',
    // themeSystem: 'bootstrap4',
    now: moment().format('YYYY-MM-DD'),
    editable: true,
    aspectRatio: 3.3,
    height: 'auto',
    monthNames: ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
    slotLabelFormat: [
      'MMMM YYYY', // top level of text
      'D'        // lower level of text
    ],
    scrollTime: '00:00',
    header: {
      left: 'today prev,next',
      center: 'title',
      right: (headerRightAppend ? `${headerRightAppend},` : '') + 'timelineMonth,timelineQuartal,timelineYear,timeline52Weeks'
    },
    defaultView: 'timeline52Weeks',
    views: {
      timelineQuartal: {
        type: 'timeline',
        buttonText: 'Quartal',
        duration: { weeks: 13 }
      },
      timeline52Weeks: {
        type: 'timeline',
        buttonText: 'Wochen',
        duration: { years: 1 },
        slotDuration: { weeks: 1 },
        slotLabelFormat: ['MMMM YYYY', 'WW']
      },
      timelineQ4: {
        type: 'timeline',
        buttonText: 'Q4',
        duration: { year: 1 },
        slotDuration: { month: 3 },
        slotLabelFormat: ['Q YYYY', ' ']
      }
    },
    navLinks: true,
    slotWidth: 20,
    nowIndicator: true,
    resourceAreaWidth: '15%',
    resourceLabelText: 'Produkt'
  }
}
