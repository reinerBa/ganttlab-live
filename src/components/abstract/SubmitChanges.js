/* eslint no-extend-native: ["error", { "exceptions": ["String"] }] */

const hasWriteProtection = function () {
  if (this.$store.state.schedulerReadonly) {
    this.$toasted.show(`Schreibschutz aktiv!`, {className: ''})
    return true
  }
  return false
}

const editMilestones = {
  async postMilestone (ms, projectId) {
    let start = ms.start.format()
    let end = ms.end.format()
    let myNewMilestone = await this.GitLabAPI.requestAsync(`/projects/${projectId}/milestones/${ms.id}?due_date=${end}&start_date=${start}`, 'put')
    this.$toasted.show(`Projekt: ${myNewMilestone.title} gespeichert`)
    this.$store.commit('adjustMilestone', {id: ms.id, start, end})
    return myNewMilestone
  },
  async closeMilestone (ms, projectId) {
    let myNewMilestone = await this.GitLabAPI.requestAsync(`/projects/${projectId}/milestones/${ms.id}?state_event=close`, 'put')
    this.$toasted.show(`Projekt: ${myNewMilestone.title} geschlossen`)
    this.$store.commit('adjustMilestone', {id: ms.id, stateStatus: 'closed'})
    return myNewMilestone
  },
  hasWriteProtection
}

const editIssues = {
  async postIssue (is, projectId) {
    try {
      let params = `${is.id}?`
      if (is.end) params += 'due_date=' + is.end.format() + '&'
      if (is.start) params += 'start_date=' + is.start.format() + '&'
      if (is.assignees) params += 'assignee_id=' + is.assignees.join(',')
      else if (is.assignee) params += 'assignee_id=' + is.assignee

      this.GitLabAPI.setVersion(3)
      setTimeout(() => this.GitLabAPI.setVersion(4), 1e3)
      let updatedIssue = await this.GitLabAPI.requestAsync(`/projects/${projectId}/issues/${params}`, 'put')
      this.GitLabAPI.setVersion(4)
      this.$toasted.show(`Aufgabe : ${updatedIssue.title} gespeichert`)
      this.$store.commit('adjustIssue', updatedIssue)
      return updatedIssue
    } catch (reason) {
      throw reason
    }
  },
  async unassignIssue (id, projectId) {
    try {
      let myNewIssue = await this.GitLabAPI.requestAsync(`/projects/${projectId}/issues/${id}?assignee_ids=0`, 'put')
      this.$toasted.show(`Aufgabe: ${myNewIssue.title} von Mitarbeiter entfernt`)
      this.$store.commit('unassignIssue', {id})
      return myNewIssue
    } catch (reason) {
      throw reason
    }
  },
  async moveIssue (is, projectId, targetProjectId) {
    try {
      let value = await this.GitLabAPI.requestAsync(`/projects/${projectId}/issues/${is.id}/move?to_project_id=${targetProjectId}`, 'post')
      this.$toasted.show(`Aufgabe: ${is.title} verschoben`)
      // this.$store.commit('moveIssue', {id: is.id, projectId, targetProjectId})
      return value
    } catch (reason) {
      this.$toasted.show(`Verschieben von: ${is.title} wegen Fehler nicht möglich`)
      throw reason
    }
  }
}

export {editMilestones, editIssues, hasWriteProtection}

String.prototype.capitalize = function () {
  return this.charAt(0).toUpperCase() + this.slice(1)
}
