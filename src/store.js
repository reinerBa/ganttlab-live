import getters from './store/getters'
import actions from './store/actions'
import createPersistedState from 'vuex-persistedstate'

export default {
  plugins: [createPersistedState({paths: ['schedulerReadonly', 'language', 'showMilestonesforDeveloper', 'activeView', 'projectsToFilter', 'developersToFilter', 'subProjects', 'labelsToHide']})],
  state: {
    url: null,
    token: null,
    provider: null,
    loginFailed: false,
    user: {
      name: null,
      avatarUrl: null,
      id: 0
    },
    activeView: '',
    tasks: null,
    projects: [],
    milestones: [],
    developers: [],
    allIssues: [],
    labels: [],
    pagination: {
      page: 1,
      perPage: 100,
      links: {
        prev: null,
        next: null
      }
    },
    schedulerReadonly: false,
    language: 'De',
    showMilestonesforDeveloper: false,
    projectsToFilter: [],
    developersToFilter: [],
    subProjects: [],
    labelsToHide: []
  },
  mutations: {
    toogleInArray (state, {property, value}) {
      if (state[property].includes(value)) {
        let idx = state[property].indexOf(value)
        state[property].splice(idx, 1)
      } else {
        state[property].splice(0, 0, value)
      }
    },
    tooggleInSubprojects (state, {id, moduleName, newSubProject}) {  // toggles values in an array of objects
      let subProject = state.subProjects.find(e => e.id === id)
      if (!subProject) return state.subProjects.push(newSubProject)

      let moduleIdx = subProject.modules.indexOf(moduleName)
      if (moduleIdx !== -1) {
        subProject.modules.splice(moduleIdx, 1)
      } else {
        subProject.modules.push(moduleName)
      }
    },
    unassignIssue (state, {id}) {
      state.allIssues
        .filter(i => i.id === Number(id))
        .forEach(i => { i.assignee = null })
    },
    adjustIssue (state, issue) {
      let oldIssue = state.allIssues.find(e => e.id === issue.id)
      for (let key in issue) oldIssue[key] = issue[key]  // may break reacitvity when assigning an assignees array instead of an assigne string
    },
    adjustMilestone (state, {id, start, end, stateStatus}) {
      state.milestones
        .filter(m => m.id === Number(id))
        .map(ms => {
          if (start) ms.start = ms.start_date = start
          if (end) ms.end = ms.due_date = end
          if (state) ms.state = stateStatus
          delete ms.color
          // ms.start = !start ? ms.start : typeof start.format === 'function' ? start.format() : start
          // ms.end = !end ? ms.end : typeof end.format === 'function' ? end.format() : end
        })
    },
    setProvider (state, value) {
      state.provider = value
    },
    setActiveView (state, value) {
      state.activeView = value
    },
    setUserId (state, value) {
      state.user.id = value
    },
    pushTo (state, {property, value}) {
      if (Array.isArray(state[property])) state[property].push(value)
    },
    resetArray (state, {property, values}) {
      state[property].splice('deleteCount')
      state[property].push(...(values || []))
    },
    set (state, {property, value}) {
      state[property] = value
    },
    clearScheduler (state) {
      state.projects = []
      state.milestones = []
      state.allMilestones = []
      state.developers = []
      state.issues = []
    },
    url (state, value) {
      state.url = value
    },
    token (state, value) {
      state.token = value
    },
    loginFailed (state, value) {
      state.loginFailed = value
    },
    user (state, value) {
      state.user = value
    },
    tasks (state, value) {
      state.tasks = value
    },
    pagination (state, value) {
      state.pagination = value
    }
  },
  actions,
  getters
}
