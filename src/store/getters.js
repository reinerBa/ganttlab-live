export default {
  downloading: state => {
    if (typeof state.GitLabAPI !== 'undefined') {
      return state.GitLabAPI.downloading
    } else if (typeof state.GitHubAPI !== 'undefined') {
      return state.GitHubAPI.downloading
    } else {
      return false
    }
  },
  noEdit: state => {
    return state.schedulerReadonly
  },
  filteredProjects: state => {
    return state.projects.filter(e => !state.projectsToFilter.includes(e.id))
  },
  hiddenProjectNum: state => state.projectsToFilter.length,
  hiddenDevelopersNum: state => state.developersToFilter.length,
  hiddenLabelsNum: state => state.labelsToHide.length,
  getModuleForProject: (state) => (id) => {
    let subProject = state.subProjects.find(p => p.projectId === id)
    if (!subProject) {
      return []
    } else {
      return subProject.modules.map(m => { return {title: m, id: `${id}_${m}`} })
    }
  },
  getProjectById: (state) => (id) => {
    return state.projects.find(p => p.id === id)
  },
  getAllSubModules: (state) => {
    let rArray = []
    state.subProjects.forEach(s => rArray.push(...s.modules.map(m => m)))
    return rArray
  },
  doesModuleExist: (state, getters) => (moduleName) => {
    return getters.filteredProjects.some(p => p.children.some(c => c.title === moduleName))
  },
  filteredDevelopers: state => {
    return state.developers.filter(e => !state.developersToFilter.includes(e.id))
  },
  filteredIssues: state => {
    return state.allIssues.filter(e => {
      let validProject = !state.projectsToFilter.includes(e.project_id)
      if (validProject && e.assignee) {
        return !state.developersToFilter.includes(e.assignee.id)
      }
      return validProject
    })
  }
}
