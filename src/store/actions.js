export default {
  async loadProjects (context) {
    let myProjects = await context.state.GitLabAPI.api.getAsync('/projects?membership=true')
    myProjects.forEach(p => {
      p.title = p.path_with_namespace.replace('/', ': ').capitalize()

      p.children = context.getters.getModuleForProject(p.id)
    })
    context.commit('set', {property: 'projects', value: myProjects})
    return myProjects
  },
  async loadUsers (context) {
    let myDevelopers = await context.state.GitLabAPI.api.getAsync('/users?active=true')
    myDevelopers.forEach(e => { e.title = e.name })

    myDevelopers = myDevelopers.filter(e => e.username !== 'root')
    context.commit('set', {property: 'developers', value: myDevelopers})
    return myDevelopers
  },
  async loadIssues (context) {
    let myIssues = await context.state.GitLabAPI.api.getAsync('/issues?scope=all&state=opened')
    myIssues.forEach(e => { e.eventType = 'issue' })
    context.commit('set', {property: 'allIssues', value: myIssues})
    let labels = []
    myIssues.map(e => labels.push(...e.labels))
    context.commit('set', {property: 'labels', value: labels})

    return myIssues
  },
  async createIssue (context, {projectId, issue}) {
    try {
      let param = 'title=' + issue.title
      param += issue.description ? '&description=' + issue.description : ''
      param += issue.assignee_ids ? '&assignee_ids=' + issue.assignee_ids.join(',') : ''
      param += issue.labels ? '&labels=' + issue.labels : ''
      param += issue.dueDate ? '&due_date=' + issue.dueDate : ''

      return await context.state.GitLabAPI.api.requestAsync(`/projects/${projectId}/issues?${param}`, 'post')
    } catch (exception) {
      return false
    }
  }
}
