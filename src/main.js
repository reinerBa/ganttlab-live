import Vue from 'vue'

import Vuex from 'vuex'
Vue.use(Vuex)

import './components/providers/WorkingBar' // this module exports a globaly installed component
import './components/providers/FormOverlay' // this module exports a globaly installed component
import './components/providers/DivDropdown'
import VueFormGenerator from 'vue-form-generator'
Vue.use(VueFormGenerator)

import Transitions from 'vue2-transitions'
Vue.use(Transitions)

import ClickOutside from 'vue-click-outside'
Vue.directive('click-outside', ClickOutside)

import Multiselect from 'vue-multiselect'
Vue.component('multiselect', Multiselect)

import VueResource from 'vue-resource'
Vue.use(VueResource)

import GitHubAPI from 'vue-github-api'
Vue.use(GitHubAPI)
import GitLabAPI from './components/providers/VueGitlabApi'
Vue.use(GitLabAPI)

import i18n from './i18n/i18nFilter'

import contextMenu from 'vue-context-menu'
Vue.component('context-menu', contextMenu)

import VModal from 'vue-js-modal'
Vue.use(VModal)

import Toasted from 'vue-toasted'
Vue.use(Toasted, {
  theme: 'outline',
  position: 'top-right',
  duration: 5000,
  className: 'toaster-class'
})
import eventManager from 'vue-event-manager'
Vue.use(eventManager) // great and short: http://vuetips.com/global-event-bus

import vuexStore from './store'
const store = new Vuex.Store(vuexStore)
Vue.use(i18n, {defaultLanguage: 'de'})

// import vuexI18n from 'vuex-i18n'
// Vue.use(vuexI18n.plugin, store)

import App from './App'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  template: '<App/>',
  components: { App },
  store
})
