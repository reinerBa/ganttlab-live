(function () {
  function install (Vue, options) {
    // default GitLab instance url, without trailing slash
/*    var url = 'https://gitlab.com'
    // GitLab user Private Token or Personal Access Token
    var token = 'you MUST provide an user Private Token or Personal Access Token'
    var headers = {
      'PRIVATE-TOKEN': token
    }
    // Gitlab api version
    var apiVersion = 'v4'
    // reading options
    if (typeof options !== 'undefined') {
      // a token is mandatory to connect to GitLab API,
      // but it could be set later with .setToken
      if (typeof options.token !== 'undefined') {
        token = options.token
        headers['PRIVATE-TOKEN'] = token
      }

      // an url may not be mandatory if user runs GitLab from localhost
      if (typeof options.url !== 'undefined') {
        // sanitizing url coming from options with no trailing slash
        url = options.url.replace(/\/$/, '')
      }
    }

    // GitLab API full url
    // var apiUrl = url + '/api/' + apiVersion

    // adding an instance method by attaching it to Vue.prototype
     */
    const Translator = {
      get: function (uri, params, fillIn, errorCb) {
        return this._updateStore(uri, params, 'get', fillIn, errorCb)
      },
      /**
       * Register your application Vuex store to improve it with Vuex store module
       * @param  {Object} store Your application Vuex store
       * @example
       * // from within a .vue component
       * this.GitLabAPI.registerStore(this.$store)
       */
      registerStore: function (store) {
        if (typeof store === 'undefined') {
          console.error('[vue-gitlab-api] This do not appear to be a Vuex store')
          return
        }

        // registering GitLabAPI Vuex module
        store.registerModule('i18n', {
          state: {
            currentLang: '',
            // is GitLabAPI currently downloading?
            downloading: false
          },
          mutations: {
            setLang (state, language) {
              // currently downloading
              state.currentLang = language
            },
            downloaded: function (state) {
              // stopped downloading
              state.running--
              state.requestMethod = ''
              if (state.running === 0) {
                state.downloading = false
              }
            }
          }
        })

        // will need to update it later
        this._vuexStore = store
      },

      /**
       * If attached, the application wide Vuex store
       * @type {Object}
       * @private
       */
      _vuexStore: null,

      /**
       * Update the Vuex store if any
       * @param  {String} mutation The mutation to commit (downloading or downloaded)
       * @param  {String=} method Optional, the that is used for downloading/uploading
       * @private
       */
      _updateStore: function (mutation, method) {
        if (this._vuexStore != null) {
          this._vuexStore.commit(mutation, method)
        }
      }
    }
    Vue.prototype.translate = Translator
  }

  if (typeof exports === 'object') {
    module.exports = install
  }
})()
